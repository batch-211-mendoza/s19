// What are conditional statements?
	// Conditional Statements allow us to control the flow of our program
	// It allwos us to run a statement/instruction if a condition is met or run another separate instruction if otherwise

	//if, else if, and else statement
	let numA = -1;

	//if statement
		//executes a statement if a specified condition is true

		if (numA < 0){
			console.log("Yes! It is less than zero");
		}

		/*
			Syntax:
			if (condition){
				statement
			}
		*/
		//the result of the expression added in the if's condition must result to true, or else the statement inside if() will not run
		console.log(numA<0); //results to true -- the if statement will run

		numA = 0;

		if ( numA < 0 ){
			console.log("Hello");
		}
		console.log(numA<0); //result to false -- the if statement will not run

		let city = "New York";
		if (city === "New York"){
			console.log("Welcome to New York City!");
		}
	
	//else-if clause
		//executes a statement if the previous conditions are false and if the specified condition is true
		//the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

		let numH = 1;
		if (numA < 0){
			console.log("Hello");
		} else if (numH > 0){
			console.log("World");
		}

		//we were able to run the else if() statement after we evaluated that the if condition was failed

		//if the if() condition was passed and ran, we will no longer evaluate the else/else if() and end the process there

		numA = 1;

		if (numA > 0){
			console.log("Hello");
		} else if (numH > 0){
			console.log("World");
		}

		//else if() statement was no longer ran because the if statement was able to run, the evaluation of the whole statement stops there

		city = "Tokyo";

		if (city === "New York"){
			console.log("Welcome to New York City!");
		} else if(city === "Tokyo"){
			console.log("Welcome to Tokyo, Japan!");
		}

		//since we failed the condition for the first if(), we went to the else if() and checked and instead passed that condition

	//else statement
		// - executes a statement if all other conditions are false
		// - the "else" statement is optional and can be added to capture any other result to change the flow of a program

		if(numA > 0){
			console.log("Hello");
		} else if(numH === 0){
			cosole.log("World");
		} else{
			console.log("Again");
		}
		/*
			Since both the preceeding if and else if conditions are not med and failed, the else statement was ran instead

			Else statements should only be added if there is a preceeding if condition.
			Else statements by itself will not work, howeverm if statements will work even if there is no else statement
		*/

		// else {
		// 	console.log("Will not run without an if");
		// }

		// else if (numH === 0){
		// 	console.log("World");
		// } else {
		// 	console.log("Again");
		// }
			//same goes for an else if, there should be a preceeding if() first

	//if, else if, and else statements with functions
		//-most of the times, we would like to use if, else if, and else statements with functions to control the flow of our application
		
		let message = "No message";
		console.log(message);

		function determineTyphoonIntensity(windSpeed){
			if(windSpeed < 30){
				return "Not a typhoon yet.";
			} else if (windSpeed <= 61){
				return "Tropical depression detected.";
			} else if (windSpeed >= 62 && windSpeed <= 88){
				return "Tropical storm detected.";
			} else if (windSpeed >= 89 && windSpeed <= 117){
				return "Severe tropical storm detected.";
			} else {
				return "Typhoon detected";
			}
		}
		//returns the string to the variable "message" that invoked it
		message = determineTyphoonIntensity(110);
		console.log(message);

		if (message == "Severe tropical storm detected."){
			console.warn(message);
		}
		//console.warn() is a good way to print warnings in our console that could help us developers act on certain output with our code

	/*
		Mini-Activity #1
		Create a function that can check whether a number is odd or even called oddOrEvenChecker with one parameter
			- A number must be provided as the argument
			- Use the if and else statement
			(Hint: use the modulo operator and strict equality operator)
			- It should not return anything
			- There should be an alert if a condition is met
			- Invoke and pass 1 argument to the oddOrEvenChecker function
	*/

	function oddOrEvenChecker(num){
		if(num%2 === 0){
			alert("The number " + num + " is an even number.");
		} else{
			alert("The number " + num + " is an odd number.");
		}
	}
	oddOrEvenChecker(6);

	/*
		Mini-Activity #2
		Check a function that can check whether a certain age is underage called ageChecker
			- A number must be provided as the argument
			- Use the if and else statements
			(Hint: use a relational operator)
			- There should be an alert if a condition is met
			- And will return a boolean value
			- Outside the function create an isAllowedToDrink variable that should be able to receive and store the result of the checkAge function
			- Log the value of the isAllowedToDrink variable in the console.
	*/

	function ageChecker(age){
		if(age >= 18){
			//return(alert("You are " + age + " years old. You are allowed to drink!"));
			// alert( age + " is allowed to drink.")
			// return true;
		} else{
			//return(alert("You are " + age + " years old. You are NOT allowed to drink!"));
			// alert( age + " is allowed to drink.")
			// return false;
		}
	}
	let isAllowedToDrink = ageChecker(14);
	console.log(isAllowedToDrink);

	/*
		- In JavaScript, a "truthy" value is a value that is considered true when encountered in Boolean context
		- Values in are considered true unless defined otherwise
		- Falsy Values/exceptions for truth:
		1. 
		2. 
		3. 
		4. 
		5. 
		6. undefined
		7. NaN
	*/

		//Truthy Examples
		/*
			-If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
			-Expressions are any unit of code that can be evaluated to a value
		*/

		if(true){
			console.log("Truthy");
		}

		if (1){
			console.log("Truthy")
		}

		if([]){
			console.log("Truthy")
		}

		//Falsy Examples

		if (false){
			console.log("Falsy")
		}

		if (0){
			console.log("Falsy")
		}
		
		if (undefined){
			console.log("Falsy")
		}

	//Conditional (Ternary) Operator
	/*
		-the Conditional (Ternary) Operator takes in three operands:
			1. condition
			2. expression to execute if the condition is truthy
			3. expression to execute if the condition is falsy
		
		- can be used as an alternative to an "if else" statement
		- ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable

		Syntax:
			(expression) ? ifTrue : ifFalse;
	*/

		//Single statement execution

	let ternaryResult = ( 1 < 18 ) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult);
	console.log(ternaryResult);


		//Multiple Statement Execution
		// a function may be defined then used in a ternary operator

		let name;

		function isOfLegalAge(){
			name = "John";
			return "You are of a legal age limit";
		}

		function isUnderAge(){
			name = "Jane";
			return "You are under the age limit";
		}

		let age = parseInt(prompt("What is your age?"));
		console.log(age);
		let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
		console.log("Result of Ternary Operator in functions " + legalAge + ", " + name);

	//Switch Statement
	/*
		the switch statement evaluates an expression and matches the expression's value to a case clause

		- .toLowerCase() function will change the input received from the prompt into all loweercase letters ensuring a match with the switch case
		- the "expression" is the information used to match the "value" provided in the switch cases
		- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values

	*/

	// let day = prompt("What day of the week is it today?").toLowerCase();
	// console.log(day);

	// switch (day){
	// 	case "monday":
	// 		console.log("The color of the day is red");
	// 		break;
	// 	case "tuesday":
	// 		console.log("The color of the day is orange");
	// 		break;
	// 	case "wednesday":
	// 		console.log("The color of the day is yellow");
	// 		break;
	// 	case "thursday":
	// 		console.log("The color of the day is green");
	// 		break;
	// 	case "friday":
	// 		console.log("The color of the day is blue");
	// 		break;
	// 	case "saturday":
	// 		console.log("The color of the day is indigo");
	// 		break;
	// 	case "sunday":
	// 		console.log("The color of the day is violet");
	// 		break;
	// 	default:
	// 		console.log("Please input a valid day");
	// }

	/*
		Mini-Activity #3

		Create a function that can determnine a bear's name with a number called determineBear
			- inside the function declare a variable named "bear" without initialization
			- inside the function add a switch to check the bear's number, add 3 cases and a default
				- if the bearNumber is 1, show an alert with the following message:
				"Hi, I'm Amy!"
				- if the bearNumber is 2, show an alert with the following message:
				"Hi, I'm Lulu!"
				- if the bearNumber is 3, show an alert with the following message:
				"Hi, I'm Morgan!"
				- if if the user's role does not fall under any of the cases, as a default, show a message:
				"bearNumber + ' is out of bounds!"
			- Invoke and pass 1 argument 
	*/

	function determineBear(bearNumber){
		let bear;
		switch(bearNumber){
			case 1:
				alert("Hi, I'm Amy");
				break;
			case 2:
				alert("Hi, I'm Lulu");
				break;
			case 3:
				alert("Hi, I'm Morgan");
				break;
			default:
				alert(bearNumber + " is out of bounds!");
				break;
		}
		return bear;
	}
	determineBear(2);

	//Try-Catch-Finally Statement
	/*
		-"try catch" statements are commonly used for error handling
		-they are used to specify a response whenever an error is received
	*/

	function showIntensityAlert(windSpeed){
		try {
			alert(determineTyphoonIntensity(windSpeed))
		} catch (error){
			console.log(typeof error);
			console.warn(error.message);
		} finally{
			alert("Intensity updates will show new alert.")
		}
	}
	showIntensityAlert(56);